class ExtendableError extends Error {
  constructor(...args) {
    super(...args)
    Error.captureStackTrace(this, this.constructor)
    this.name = this.constructor.name;
  }
}

module.exports = ExtendableError
