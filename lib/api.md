## Public
The __API__ these classes provide will not (or should not) be changing drastically unless by major feature upgrade.

* [easyDi](index.js.html)

## Private
All of the core functionality of the library is maintained in these private classes. 
As such the __API__ these classes provide will be affected more often than not.

* [Graph](Graph.html)
* [Container](Container.html)
* [Parser](Parser.html)
