const ExtendableError = require('./ExtendableError')

class MainDependencyError extends ExtendableError {
  constructor() {
    super('Main dependency not loaded - cannot resolve graph')
  }
}

class NoExistingContainerError extends ExtendableError {
  constructor(containerName) {
    super(`Container ${ containerName } does not exist - unable to resolve`)
  }
}

module.exports = {
  MainDependencyError,
  NoExistingContainerError
}
