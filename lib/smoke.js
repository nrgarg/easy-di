const easyDi = require('./')

describe('SmokeTest', () => {
  describe('simple module resolution', () => {
    let containerName, container
    beforeEach(() => {
      containerName = 'app'
      container = easyDi.module(containerName)
      container
        .define(function one() {
          return 'one'
        })
        .define(function three(two) {
          return 'three AND ' + two
        })
        .define(function two(one) {
          return 'two AND ' + one
        })
    })

    afterEach(() => {
      container.reset()
    })

    it('executes without failure', () => {
      container
        .main((one, two, three) => {
          expect(one).to.eql('one')
          expect(two).to.eql('two AND one')
          expect(three).to.eql('three AND two AND one')
        })
        .resolve()
    })
  })

  describe('shared module resolution', () => {
    let containerName1, containerName2, containerName3, container1, container2, container3
    beforeEach(() => {
      containerName1 = 'app'
      containerName2 = 'lib1'
      containerName3 = 'lib2'
      container1 = easyDi.module(containerName1)
      container2 = easyDi.module(containerName2)
      container3 = easyDi.module(containerName3)

      container1
        .define(function three(two) {
          return 'three AND ' + two
        })

      container2
        .define(function one() {
          return 'one'
        })

      container3
        .define(function two() {
          return 'two'
        })
    })

    afterEach(() => {
      container1.reset()
      container2.reset()
      container3.reset()
    })

    it('executes without failure', () => {
      container1
        .include(containerName2, containerName3)
        .main((one, two, three) => {
          expect(one).to.eql('one')
          expect(two).to.eql('two')
          expect(three).to.eql('three AND two')
        })
        .resolve()
    })
  })

  describe('using single dependency in main', () => {
    let container, containerName
    beforeEach(() => {
      containerName = 'app'
      container = easyDi.module(containerName)

      container
        .define(function one() {
          return 'one'
        })
    })

    afterEach(() => {
      container.reset()
    })

    it('executes without failure', () => {
      container
        .main(one => {
          expect(one).to.eql('one')
        })
        .resolve()
    })
  })

  describe('wrong dependency naming', () => {
    let container, containerName
    beforeEach(() => {
      containerName = 'app'
      container = easyDi.module(containerName)
      container
        .define(function One() {
        })
        .main(one => {
        })
    })
    afterEach(() => {
      container.reset()
    })

    it('throws an error', () => {
      expect(() => {
        container.resolve()
      }).to.throw()
    })
  })
})
