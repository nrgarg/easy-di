const Container = require('./')
const Graph = require('./Graph')
const Parser = require('./Parser')
const {
  MainDependencyError,
  NoExistingContainerError
} = require('./errors')

describe('Container', () => {
  describe('.module', () => {
    it('returns an instance of a Container', () => {
      expect(Container.module('test')).to.be.instanceof(Container)
    })
  })

  describe('.resolve', () => {
    describe('when the container exists', () => {
      let moduleName
      beforeEach(() => {
        moduleName = 'test'
        Container
          .module(moduleName)
          .main(() => {})
      })

      describe('when the main dependency is defined', () => {
        let spy
        beforeEach(() => {
          spy = sinon.spy(Container.module(moduleName), 'resolve')
        })

        afterEach(() => {
          Container.module(moduleName).resolve.restore()
        })

        it('resolves the container', () => {
          Container.module(moduleName).resolve()
          expect(spy.called).to.eql(true)
        })
      })

      describe('when the main dependency is not defined', () => {
        let moduleName
        beforeEach(() => {
          moduleName = 'test2'
        })

        it('throws MainDependencyError', () => {
          Container.module(moduleName)

          expect(() => {
            Container.module(moduleName).resolve()
          }).to.throw(MainDependencyError)
        })
      })
    })

    describe('when the container does not exist', () => {
      let moduleName
      beforeEach(() => {
        moduleName = 'test3'
      })
      it('throws NoExistingContainerError', () => {
        expect(() => {
          Container.resolve(moduleName)
        }).to.throw(NoExistingContainerError)
      })
    })
  })
  describe('#define', () => {
    let container, spy, depName
    beforeEach(() => {
      container = new Container()
      depName = 'test'
      spy = sinon.spy(container.graph, 'add')
    })
    it('calls graph#add', () => {
      container.define(function test() {})
      expect(spy.called).to.eql(true)
    })
  })

  describe('#main', () => {
    let container, main
    beforeEach(() => {
      container = new Container()
      main = () => {}
    })

    it('sets the main property', () => {
      container.main(main)
      expect(container._main).to.eql({ deps: [], fn: main})
    })
  })

  describe('#resolve', () => {
    describe('when main is set', () => {
      let container, main, resolveAllSpy, resolveNodeSpy
      beforeEach(() => {
        container = new Container()
        main = () => {}
        container.main(main)
        resolveAllSpy = sinon.spy(container.graph, 'resolveAllNodes')
        resolveNodeSpy = sinon.spy(container.graph, 'resolveNode')
      })

      it('calls graph#resolveAllNodes', () => {
        container.resolve()
        expect(resolveAllSpy.called).to.eql(true)
      })

      it('calls graph#resolveNode', () => {
        container.resolve()
        expect(resolveNodeSpy.called).to.eql(true)
      })
    })

    describe('when main is not set', () => {
      let container
      beforeEach(() => {
        container = new Container()
      })

      it('throws MainDependencyError', () => {
        expect(() => {
          container.resolve()
        }).to.throw(MainDependencyError)
      })
    })
  })

  describe('#reset', () => {
    let container, graph, parser
    beforeEach(() => {
      container = new Container()
      graph = container.graph
      parser = container.parse
      container.reset()
    })

    it('sets the main injector to null', () => {
      expect(container._main).to.eql(null)
    })

    it('sets external dependencies to empty array', () => {
      expect(container.external).to.eql([])
    })

    it('sets a new graph instance', () => {
      expect(container.graph).not.to.equal(graph)
      expect(container.graph).to.be.an.instanceof(Graph)
    })

    it('sets a new parser instance', () => {
      expect(container.parse).not.to.equal(parser)
      expect(container.parse).to.be.an.instanceof(Parser)
    })
  })
})