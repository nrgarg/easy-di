const ARROW_ARG = /^([^(]+?)=>/
const FN_ARGS = /^[^(]*\(\s*([^)]*)\)/m
const FN_ARG_SPLIT = /,/
const FN_ARG = /\s*(.*)\s*/
const STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg
const STRING_SPACE = /\s/mg

const {
  InvalidDependencyDefinitionError,
  NoDependencyNameError
} = require('./errors')

/**
 * @class Parser
 */
class Parser {
  /**
   * @typedef {object} GraphNode
   * @property deps {Array.string} array of dependencies needed for the node
   * @property fn {Function} the function to execute when all dependencies have been provided
   */

  /**
   * @typedef {object} NamedGraphNode
   * @property name {String} name of node
   * @property deps {Array.string} array of dependencies needed for the node
   * @property fn {Function} the function to execute when all dependencies have been provided
   */

  /**
   * @param injectorFn {Function} injector function
   * @returns {NamedGraphNode}
   */
  dependencyDefinition(injectorFn) {
    if (typeof injectorFn !== "function") {
      throw new InvalidDependencyDefinitionError(typeof injectorFn)
    }
    if (injectorFn.name.length <= 0) {
      throw new NoDependencyNameError()
    }
    return {
      name: injectorFn.name,
      deps: this.args(injectorFn.toString()),
      fn: injectorFn
    }
  }

  /**
   *
   * @param injectorFn {Function} injector function
   * @returns {GraphNode}
   */
  mainDefinition(injectorFn) {
    if (typeof injectorFn !== "function") {
      throw new InvalidDependencyDefinitionError(typeof injectorFn)
    }

    return {
      deps: this.args(injectorFn.toString()),
      fn: injectorFn
    }
  }

  /**
   *
   * @param injectorString {String} stringified function
   * @returns {Array.string}
   *
   * @desc
   * Parses a function string and returns the function arguments as an array of strings
   */
  args(injectorString) {
    const fnText = injectorString.replace(STRIP_COMMENTS, '').replace(STRING_SPACE, '')
    const [, args] = fnText.match(ARROW_ARG) || fnText.match(FN_ARGS)
    return args
      .split(FN_ARG_SPLIT)
      .map(arg => arg.match(FN_ARG)[1])
      .filter(str => str.length)
  }
}

module.exports = Parser
