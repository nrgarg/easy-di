const ExtendableError = require('../ExtendableError')

class InvalidDependencyDefinitionError extends ExtendableError {
  constructor(type) {
    super(`Parser expects dependency definition of type "function" - received ${type}`)
  }
}

class NoDependencyNameError extends ExtendableError {
  constructor() {
    super(`Parser expects non-empty dependency definition name`)
  }
}

module.exports = {
  InvalidDependencyDefinitionError,
  NoDependencyNameError
}
