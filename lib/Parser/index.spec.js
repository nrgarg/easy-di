const Parser = require('./')
const {
  InvalidDependencyDefinitionError,
  NoDependencyNameError
} = require('./errors')

describe('Parser', () => {
  describe('#args', () => {
    describe('using function declaration', () => {
      describe('when no parameters exist', () => {
        let parser, fnString
        beforeEach(() => {
          parser = new Parser()
          fnString = 'function () {}'
        })
        it('returns an empty array', () => {
          return expect(parser.args(fnString)).to.be.empty
        })

        describe('and contains comments', () => {
          beforeEach(() => {
            fnString = 'function (/**comment**/) {}'
          })

          it('returns an empty array', () => {
            return expect(parser.args(fnString)).to.be.empty
          })
        })
      })

      describe('when parameters exist', () => {
        let parser, fnString, arg1, arg2
        beforeEach(() => {
          parser = new Parser()
          arg1 = 'test1'
          arg2 = 'test2'
          fnString = `function (${arg1}, ${arg2}) {}`
        })
        it('returns an array of argument names', () => {
          return expect(parser.args(fnString)).to.deep.equal([arg1, arg2])
        })

        describe('and contains comments', () => {
          beforeEach(() => {
            fnString = `function (${arg1},/** comment **/ ${arg2}) {}`
          })

          it('returns an empty array', () => {
            return expect(parser.args(fnString)).to.deep.equal([arg1, arg2])
          })
        })
      })
    })
    describe('using arrow functions', () => {
      describe('when no parameters exist', () => {
        let parser, fnString
        beforeEach(() => {
          parser = new Parser()
          fnString = '() => {}'
        })
        it('returns an empty array', () => {
          return expect(parser.args(fnString)).to.be.empty
        })

        describe('and contains comments', () => {
          beforeEach(() => {
            fnString = '(/**comment**/) => {}'
          })

          it('returns an empty array', () => {
            return expect(parser.args(fnString)).to.be.empty
          })
        })
      })

      describe('when multiple parameters exist', () => {
        let parser, fnString, arg1, arg2
        beforeEach(() => {
          parser = new Parser()
          arg1 = 'test1'
          arg2 = 'test2'
          fnString = `(${arg1}, ${arg2}) => {}`
        })
        it('returns an array of argument names', () => {
          return expect(parser.args(fnString)).to.deep.equal([arg1, arg2])
        })

        describe('and contains comments', () => {
          beforeEach(() => {
            fnString = `(${arg1},/** comment **/ ${arg2}) => {}`
          })

          it('returns an empty array', () => {
            return expect(parser.args(fnString)).to.deep.equal([arg1, arg2])
          })
        })
      })

      describe('when one parameter exists', () => {
        let parser, arg1
        beforeEach(() => {
          parser = new Parser()
          arg1 = 'test1'
        })

        describe('using parenthesis', () => {
          let fnString
          beforeEach(() => {
            fnString = `(${arg1}) => {}`
          })
          it('returns an array of single argument', () => {
            return expect(parser.args(fnString)).to.deep.equal([arg1])
          })

          describe('and contains comments', () => {
            beforeEach(() => {
              fnString = `(${arg1}/** comment **/ ) => {}`
            })

            it('returns an empty array', () => {
              return expect(parser.args(fnString)).to.deep.equal([arg1])
            })
          })
        })

        describe('without parenthesis', () => {
          let fnString
          beforeEach(() => {
            fnString = `${arg1} => {}`
          })
          it('returns an array of single argument', () => {
            return expect(parser.args(fnString)).to.deep.equal([arg1])
          })

          describe('and contains comments', () => {
            beforeEach(() => {
              fnString = `${arg1}/** comment **/ => {}`
            })

            it('returns an empty array', () => {
              return expect(parser.args(fnString)).to.deep.equal([arg1])
            })
          })
        })
      })
    })
  })

  describe('#dependencyDefinition', () => {
    describe('when injector is not a function', () => {
      let parser, injector
      beforeEach(() => {
        parser = new Parser()
        injector = {}
      })

      it('throws InvalidDependencyDefinitionError', () => {
        expect(() => {
          parser.dependencyDefinition(injector)
        }).to.throw(InvalidDependencyDefinitionError)
      })
    })

    describe('when injector name is empty', () => {
      let parser
      beforeEach(() => {
        parser = new Parser()
      })

      it('throws InvalidDependencyNameError', () => {
        expect(() => {
          parser.dependencyDefinition(function () {})
        }).to.throw(NoDependencyNameError)
      })
    })

    describe('when injector is valid', () => {
      let parser, injector
      beforeEach(() => {
        parser = new Parser()
        injector = function (dep1, dep2) {}
      })
      it('returns an object containing the graph node definition', () => {
        expect(parser.dependencyDefinition(injector)).to.deep.equal({
          name: 'injector',
          deps: ['dep1', 'dep2'],
          fn: injector
        })
      })
    })
  })

  describe('#mainDefinition', () => {
    describe('when injector is not a function', () => {
      let parser, injector
      beforeEach(() => {
        parser = new Parser()
        injector = {}
      })

      it('throws InvalidDependencyDefinitionError', () => {
        expect(() => {
          parser.mainDefinition(injector)
        }).to.throw(InvalidDependencyDefinitionError)
      })
    })

    describe('when injector name is empty', () => {
      let parser
      beforeEach(() => {
        parser = new Parser()
      })

      it('does not throw NoDependencyNameError', () => {
        expect(() => {
          parser.mainDefinition(function () {})
        }).not.to.throw(NoDependencyNameError)
      })
    })

    describe('when injector is valid', () => {
      let parser, injector
      beforeEach(() => {
        parser = new Parser()
        injector = function (dep1, dep2) {}
      })
      it('returns an object containing the graph node definition', () => {
        expect(parser.mainDefinition(injector)).to.deep.equal({
          deps: ['dep1', 'dep2'],
          fn: injector
        })
      })
    })
  })
})

