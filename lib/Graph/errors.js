const ExtendableError = require('../ExtendableError')

class UnresolvedDependencyError extends ExtendableError {
  constructor(name) {
    super(`Dependency "${ name }" is either not defined in the system or returns nil (undefined/null)`)
  }
}

module.exports = {
  UnresolvedDependencyError
}
