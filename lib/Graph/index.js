const {
  UnresolvedDependencyError
} = require('./errors')

/**
 * @class Graph
 */
class Graph {
  /**
   * @constructor Graph
   */
  constructor() {
    this.edges = {}
    this.resolved = {}
  }

  /**
   * @method Graph#add
   * @param {string}    name    Name of dependency
   * @param {GraphNode} node {@link GraphNode}

   * @returns {Graph}
   * @desc
   * Adds a node by name into the graph
   */
  add(name, node) {
    this.edges[name] = node

    return this
  }

  /**
   * @method Graph#resolveAllNodes
   * @return {Graph}
   * @desc
   * Sorts the dependencies using {@link Graph#sort}.
   * Once dependency order is established, it iterates through and resolves each one
   */
  resolveAllNodes() {
    this.sort().forEach(nodeName => {
      const node = this.edges[nodeName]
      this.resolved[nodeName] = this.resolveNode(node)
    })

    return this
  }

  /**
   * @method Graph#resolveNode
   * @param {GraphNode}   node       See {@link GraphNode}
   * @returns {*} resolved node
   * @desc
   * Retrieves all of the resolved dependencies for provided nodeName
   * and then resolves the current node
   */
  resolveNode(node) {
    const deps = node.deps.map(depName => {
      const resolvedNode = this.resolved[depName]
      if (resolvedNode === undefined || resolvedNode === null) {
        throw new UnresolvedDependencyError(depName)
      }
      return resolvedNode
    })

    return node.fn(...deps)
  }


  /**
   * @method Graph#sort
   * @returns {string[]} sorted dependency list
   * @desc
   * Does a basic topological sorting of all of the dependencies
   * in the edges object. Calls {@link Graph#dfs}
   */
  sort() {
    const visited = {}
    const sorted = []

    for (const key in this.edges) {
      if (this.edges.hasOwnProperty(key) && !visited[key]) {
        this.dfs(visited, key, sorted)
      }
    }

    return sorted
  }

  /**
   * @method Graph#dfs
   * @param {object} visited      Object containing visited nodes
   * @param {string} dependency   Current dependency name
   * @param {string[]}  sorted    Array of sorted dependencies
   * @desc
   * A depth first search (utilized for topological sorting) through
   * all of the dependencies.
   */
  dfs(visited, dependency, sorted) {
    visited[dependency] = true

    this.edges[dependency].deps.forEach(dep => {
      if (!visited[dep]) {
        this.dfs(visited, dep, sorted)
      }
    })

    sorted.push(dependency)
  }

  /**
   * @method Graph#merge
   * @param otherGraph {Graph}
   *
   * @desc
   * Merge the edges from provided graph to the current graph instance
   */
  merge(otherGraph) {
    Object.assign(this.edges, otherGraph.edges)
  }
}

module.exports = Graph
