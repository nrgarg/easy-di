const Graph = require('./')
const {
  UnresolvedDependencyError
} = require('./errors')

describe('Graph', () => {
  describe('#add', () => {
    let graph, name, node
    beforeEach(() => {
      graph = new Graph()
      name = 'test'
      node = 'value'
    })

    it('sets the node by name in the graph', () => {
      graph.add(name, node)

      expect(graph.edges[name]).to.equal(node)
    })
  })

  describe('#resolveAllNodes', () => {

  })

  describe('#resolveNode', () => {
    describe('when all requested dependencies exist', () => {
      let dep1, dep2, spyDep2Fn, dep1Text, dep2Text, dep1Name, dep2Name, graph
      beforeEach(() => {
        dep1Text = 'dep1'
        dep2Text = 'dep2'
        dep1Name = 'dep1'
        dep2Name = 'dep2'
        dep1 =  dep1Text

        dep2 =  { deps: [dep1Name], fn: (dep1) => (dep1 + dep2Text) }
        spyDep2Fn = sinon.spy(dep2, 'fn')

        graph = new Graph()
        graph.resolved[dep1Name] = dep1
      })

      it('calls the injector function on the node with the resolved dependencies', () => {
        graph.resolveNode(dep2)

        expect(spyDep2Fn.called).to.equal(true)
      })
    })

    describe('when requested dependencies do not exist', () => {
      let dep1, dep2, spyDep2Fn, dep1Text, dep2Text, dep1Name, dep2Name, graph
      beforeEach(() => {
        dep1Text = 'dep1'
        dep2Text = 'dep2'
        dep1Name = 'dep1'
        dep2Name = 'dep2'
        dep1 =  dep1Text

        dep2 =  { deps: [dep1Name], fn: (dep1) => (dep1 + dep2Text) }
        spyDep2Fn = sinon.spy(dep2, 'fn')

        graph = new Graph()
      })

      it('calls the injector function on the node with the resolved dependencies', () => {
        expect(() => {
          graph.resolveNode(dep2)
        }).to.throw(UnresolvedDependencyError)
      })
    })
  })

  describe('#sort', () => {

  })

  describe('#dfs', () => {

  })

  describe('#merge', () => {
    let graph1, graph2, key1, val1, key2, val2
    beforeEach(() => {
      graph1 = new Graph()
      graph2 = new Graph()
      key1 = 'key1'
      val1 = 'val1'

      key2 = 'key2'
      val2 = 'val2'

      graph1.edges[key1] = val1

      graph2.edges[key2] = val2
    })
    it('maps edges from destination graph to source graph', () => {
      graph1.merge(graph2)

      expect(graph1.edges).to.deep.equal({
        [key1]: val1,
        [key2]: val2
      })
    })
  })
})