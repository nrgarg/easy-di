const Graph = require('./Graph')
const Parser = require('./Parser')
const {
  MainDependencyError,
  NoExistingContainerError
} = require('./errors')

const containers = {}

/**
 * @class Container
 */
class Container {
  /**
   * @param containerName {String} name of container
   * @returns {Container}
   */
  static module(containerName) {
    if (!containers.hasOwnProperty(containerName)) {
      containers[containerName] = new Container(containerName)
    }
    return containers[containerName]
  }

  /**
   * @param containerName {String} name of container
   * @returns {Container}
   */
  static resolve(containerName) {
    if (!containers.hasOwnProperty(containerName)) {
      throw new NoExistingContainerError()
    }
    return containers[containerName].resolve()
  }

  /**
   * @param containerName {String} name of container
   * @returns {Container}
   */
  static reset(containerName) {
    if (!containers.hasOwnProperty(containerName)) {
      throw new NoExistingContainerError()
    }

    return containers[containerName].reset()
  }

  /**
   * @param name {String} name of container
   */
  constructor(name) {
    this._main = null
    this.external = []
    this.name = name
    this.graph = new Graph()
    this.parse = new Parser()
  }

  /**
   * @method Container#define
   * @param injectorFn {Function} injector function
   * @returns {Container}
   *
   * @desc
   * Adds a node into the underlying graph instance
   */
  define(injectorFn) {
    const { name, deps, fn } = this.parse.dependencyDefinition(injectorFn)

    this.graph.add(name, { deps, fn })

    return this
  }

  /**
   * @method Container#setMain
   * @param injectorFn {Function} injector function
   * @returns {Container}
   *
   * @desc
   * Creates a new GraphNode and stores it in the "main" property
   */
  main(injectorFn) {
    this._main = this.parse.mainDefinition(injectorFn)

    return this
  }

  /**
   * @method Container#resolveGraph
   * @returns {Container}
   *
   * @desc
   * Resolves all of the nodes in the graph and then resolves the "main" node
   */
  resolve() {
    if (this._main === null) {
      throw new MainDependencyError()
    }
    const loadAllExternalContainers = (externalContainers, container) => {
      container.external.forEach(containerName => {
        if (!containers.hasOwnProperty(containerName)) {
          throw new NoExistingContainerError(containerName)
        }
        if (!externalContainers.has(containerName)) {
          container = Container.module(containerName)
          externalContainers.set(containerName, container)
          loadAllExternalContainers(externalContainers, container)
        }
      })

      return externalContainers
    }

    const externalContainers = loadAllExternalContainers(new Map(), this)

    externalContainers.forEach(container => {
      this.graph.merge(container.graph)
    })
    this.graph.resolveAllNodes()
    this.graph.resolveNode(this._main)

    return this
  }

  /**
   * @method Container#include
   * @param containerNames {Array.string} names of containers
   * @returns {Container}
   *
   * @desc
   * Specify the container names to include into this container
   */
  include(...containerNames) {
    this.external.push(...containerNames)

    return this
  }

  /**
   * @method Container#use
   * @alias Container#include
   */
  use(...args) {
    return this.include(...args)
  }

  /**
   * @method Container#reset
   * @returns {Container}
   *
   * @desc
   * Resets the container
   */
  reset() {
    this._main = null
    this.external = []
    this.graph = new Graph()
    this.parse = new Parser()

    return this
  }
}


module.exports = Container
